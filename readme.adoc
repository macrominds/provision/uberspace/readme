:group-base-link: https://gitlab.com/macrominds/provision/uberspace
= Provision Uberspace

A collection for automatic provisioning of https://uberspace.de[Uberspace] accounts using https://www.ansible.com[Ansible].

== Modular Features

Easy setup for mail and web with modular Ansible roles (biased towards https://www.php.net[php] websites).

=== Generally applicable

* Manage {group-base-link}/mail-domains[mail domains]
* Manage {group-base-link}/mail-accounts[mail accounts]
* Setup a {group-base-link}/mail-autoresponder[folder based mail autoresponder]
* Install {group-base-link}/lib-webp[lib webp] to convert your images to *.webp for improved compression
* Manage {group-base-link}/web-domains[web domains]
* Put a {group-base-link}/deployment-key[private deployment] key onto your server to be able to checkout private git repos

=== Biased towards php & deployer but configurable

* Manage the {group-base-link}/web-prerequisites[php version]
* Turn the Uberspace html folder into a {group-base-link}/web-link-html-to-deployer-target[symlink to your deployment target]
* Setup a {group-base-link}/web-provide-shared-deployer-dot-env[shared .env] for deployer/deployer
* Manage {group-base-link}/web-logrotate[log rotate]

== Examples

Clone & customize for your own setup.

* https://gitlab.com/macrominds/pianino/provision-uberspace[Real life php Website]
* https://gitlab.com/naturkindergarten/provision-uberspace[Real life Mail domains, accounts and autoresponder]

== Related works

* https://github.com/friesenkiwi/ansible-collection-uberspace (Very well done Uberspace account creation, management and more)



